# sc_secrets

This repository is to save the sensible passowords and data of SC.

We use:

* Git
* Ansible Vault
* Vi/Vim or other text editor

## First of all clone the repository!!!

To clone the repository in a local folder, execute:

`$ git clone git@bitbucket.org:danypr92/sc_secrets.giti secrets`

This action creates a folder named `secrets` with all the repository content.

## Read Secrets

1 - Go to the project folder

`$ cd secrets`

2 - Pull the content

`$ git pull`

3 - Execute view command: ansible-vault view <secret-file>

`$ ansible-vault view secrets.yml`

4 - Enter the password

`Vault password: `

5 - To close the file, press `q`


## Add/Edit Secrets

1 - Go to the project folder

`$ cd secrets`

2 - Pull the content

`$ git pull`

3 - Execute edit command: ansible-vault edit <secret-file>

`$ ansible-vault edit secrets.yml`

4 - Enter the password

`Vault password: `

5 - Edit the secrets file in plain text and save it with your predefined editor (vi, vim, ...)

6 - Commit the changes with a concrete commit message

`$ git commit . -m "Commit message"`

7 - Push the new changes to the remote repository

`$ git push`


## TODO

* Test [SOPS](https://github.com/mozilla/sops) to encrypt only the values
